
// Repetition Control Structures

// While Loop
// it goes first to a condition, once it is satisfied it will execute its statment.Otherwise, walang ilalabas

/* Syntax :
	while(condition){
		statement/s;

	}
*/

 // let count = 5;

 // while(count !== 0){
 // 	console.log("While :" + count);
 // 	count--;

 // }

 // console.log("Displays numbers 1-10")

 // count = 1;
 // while(count < 11){
 // 	console.log("While :" + count);
 // 	count++;

 // }

 // Do while Loop
 // -unlike while loop, it will guarantee na meron pang ieexecute na statement kahit hindi nasatisfy ang condition

 /*	 syntax:
 		do{
			statement;
 		}while (condition);

 */
// Number() is similar to ParseInt when converting str to numbers
 let number = Number(prompt("Give me a number"));

 do{
 	console.log("Do while:" + number);
 	number +=1;
 }while (number < 10);

 // Create a new var to be used in displaying even number from 2-10 using do while loop
 let evenNum = 2

 do {
 	console.log("Even :" + evenNum);
	evenNum += 2;}
	while(evenNum < 11);

// For Loop
/* 	Syntax:
	for(initialization;condition;stepExpression){
	statement;
	}

*/
console.log("For Loop")
for(let count = 0;count <=20;count++){
	console.log(count);
}

console.log("Even for Loop")
let even = 2;
for(let counter = 1; counter <=5; counter++){
	console.log("even:" + even);
	even +=2;
}

// counter var- determine on how many times magiiterate ang output

console.log("Other For Loop examples:")
let myString = 'alex';
// .length property is used to count the charact in string
console.log(myString.length);
console.log(myString[0])

for(let x = 1; x <myString.length; x++){
	console.log(myString[x]);
}
// reversed alex
// for(let y = myString.length; y > 0; y--) {
// 	console.log(myString[y - 1]);
// }


// Print out letter individually but will print 3 instead of the vowels

let myName = "AlEx";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

//Continue and Break Statements
// continue - allows continution ng loop kahit hindi iaccess yung mga statements
// break- magbbreak na buong loop pag may nagmatch sa condition

for(let count = 0;count <=20; count++){
	// if the remainder is equal to 0,tells the code to continue to iterate
	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	// if the current value of count is greater than 10, tells the code to break the loop

	if(count > 10){
		break;
	}
}

let name = 'alexandro'

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("continue to the next iteration");
		continue;
	}
	if(name[i] === 'd'){
		break;
	}
}